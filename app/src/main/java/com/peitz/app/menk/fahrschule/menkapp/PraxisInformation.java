package com.peitz.app.menk.fahrschule.menkapp;

public class PraxisInformation {

    private String title;

    public PraxisInformation(String title) {
        this.title = title;
    }

    public String getTitle() {
        return this.title;
    }
}