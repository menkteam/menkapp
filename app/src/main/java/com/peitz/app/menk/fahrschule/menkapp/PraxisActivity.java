package com.peitz.app.menk.fahrschule.menkapp;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class PraxisActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_praxis);
        // 1. get a reference to recyclerView
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        // this is data fro recycler view
        PraxisInformation itemsData[] = {
                new PraxisInformation("Praxis1"),
                new PraxisInformation("Praxis2"),
                new PraxisInformation("Praxis3"),
                new PraxisInformation("Praxis4"),
                new PraxisInformation("Praxis5"),
                new PraxisInformation("Praxis6"),
                new PraxisInformation("Praxis7"),
                new PraxisInformation("Praxis8"),
                new PraxisInformation("Praxis9"),
                new PraxisInformation("Praxis10"),
                new PraxisInformation("Praxis11"),
                new PraxisInformation("Praxis12"),
                new PraxisInformation("Praxis13"),
                new PraxisInformation("Praxis14"),
                new PraxisInformation("Praxis15")};

        // 2. set layoutManger
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // 3. create an adapter
        PraxisAdapter mAdapter = new PraxisAdapter(itemsData);
        // 4. set adapter
        recyclerView.setAdapter(mAdapter);
        // 5. set item animator to DefaultAnimator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    public void test(View v) {
        Toast.makeText(getApplicationContext(), "geklickt", Toast.LENGTH_SHORT).show();
    }
}
